package com.proctoru.kurento.prototype;

import org.springframework.stereotype.Service;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Created by stsepinaku on 14.02.2017.
 */
@Service
public class MultimediaHandler extends TextWebSocketHandler {
}

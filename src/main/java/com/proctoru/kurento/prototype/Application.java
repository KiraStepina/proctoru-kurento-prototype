package com.proctoru.kurento.prototype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by stsepinaku on 14.02.2017.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {
        new SpringApplication(Application.class).run(args);
    }

}
